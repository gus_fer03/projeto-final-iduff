import React from 'react'
import Table from '../../components/Table/Table'
import {Card} from '../../components/Card/Card';
import classes from '../Assignment/Assignment.module.css';

const columns = [
    'Nome',
    'Vagas',
    'Período',
    'CHT'
]

const data = [
    ['Cálculo 1',
    '13/45',
    '1 periodo',
    '60',
    <button className='botao' onClick={console.log}>INSCREVER</button>],
    ['Calculo 2',
    '25/30',
    '2 periodo',
    '90',
    <button className='botao' onClick={console.log}>INSCREVER</button>]
]

export function Assignment() {
    return (
        <Card title="Inscrição em matérias">
            <Table columns={columns} data={data}/>
        </Card>
    )
}
